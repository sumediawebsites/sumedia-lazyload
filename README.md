#Sumedia LazyLoad
This is a vanilla Javascript implementation of cross-browser compatible lazyloading.

## Browser support

This package relies on both `Array.from()` and `IntersectionObserver` which eren't widely available yet. For optimal browser support make sure you call the following before initiating:
```javascript
LazyLoad.polyfill()
```

## Installing

Using npm:

```bash
$ npm install sumedia-lazyload
```

Using yarn:

```bash
$ yarn add sumedia-lazyload
```

Using cdn:
```html
<<<<<<< HEAD
<script src="https://unpkg.com/sumedia-lazyload@1.0.6/lazyload.js" defer></script>
=======
<script src="https://unpkg.com/sumedia-lazyload@1.0.7/lazyload.js" defer></script>
>>>>>>> develop
```

## Usage
Before you can do anything, you need to import the package:
```javascript
import { LazyLoad } from 'sumedia-lazyload'
```

To initiate lazyloading, call `LazyLoad.init()`. For IE11 support: make sure to call `LazyLoad.polyfill()` first.

To enable lazyload apply the `.lazy-image` class to the element containing your `data-src`.

#### Example

```html
<picture>
    <source data-srcset="https://via.placeholder.com/1280/FF0000/FFF/?text=lazyload" media="(min-width: 1440px)" />
    <source data-srcset="https://via.placeholder.com/850/FF0000/FFF/?text=lazyload" media="(min-width: 1280px)" />
    <source data-srcset="https://via.placeholder.com/550/FF0000/FFF/?text=lazyload" media="(min-width: 768px)" />
    <img data-src="https://via.placeholder.com/150/FF0000/FFF/?text=lazyload" class="lazy-image">
<picture>
```