import { arrayFrom } from './util/polyfills'
import { objectAssign } from './util/polyfills'

const config = {
    dataSrcset: 'data-srcset',
    imageLoadedClass: 'lazy-image-handled',
    imageSelector: '.lazy-image',
    rootMargin: '100px 0px',
    threshold: 0.01,
}

let images
let imageCount
let observer

/**
 * Preloads the image
 * @param {object} image
 */
const preloadImage = (image) => {
    return applyImage(image)
}

/**
 * Load all of the images immediately
 * @param {NodeListOf<Element>} images
 */
const loadImagesImmediately = (images) => {
    for (let i = 0; i < images.length; i++) {
        let image = images[i]
        preloadImage(image)
    }
}

/**
 * Handle src, do `srcset` first
 * @param {object} element
 */
const handleSrc = (element) => {
    if (element.getAttribute('data-srcset') !== null) {
        element.setAttribute('srcset', element.getAttribute('data-srcset'))
    }

    if (element.getAttribute('data-src') !== null) {
        element.setAttribute('src', element.getAttribute('data-src'))
    }
}

/**
 * Disconnect the observer
 */
const disconnect = () => {
    if (!observer)
        return

    observer.disconnect()
}

/**
 * On intersection
 * @param {array} entries
 */
const onIntersection = (entries) => {
    if (imageCount === 0) {
        disconnect()
        return
    }

    for (let i = 0; i < entries.length; i++) {
        let entry = entries[i]
        if (entry.intersectionRatio > 0) {
            imageCount--

            observer.unobserve(entry.target)
            preloadImage(entry.target)
        }
    }
}

/**
 * Apply the image
 * @param {object} img
 */
const applyImage = (img) => {
    img.classList.add(config.imageLoadedClass)

    const parentElement = img.parentElement

    if (parentElement.tagName === 'PICTURE') {
        Array.from(parentElement.getElementsByTagName('source')).forEach(source => handleSrc(source))
    }

    handleSrc(img)
}

export const LazyLoad = {
    /**
     * @param {Object} [localConfig] - Object of config settings
     * @param {string} [localConfig.dataSrcset=data-srcset] - Data attribute to look for on srcsets
     * @param {string} [localConfig.imageLoadedClass] - Class to add for once images are loaded
     * @param {string} [localConfig.imageSelector] - Selector to look for when scanning page for images to lazy load
     * @param {string} [localConfig.rootMargin=100px 0px] - Bounds for IntersectionObserver
     */
    init: ( localConfig = null ) => {
        images = document.querySelectorAll(config.imageSelector)
        imageCount = images.length
        
        if (localConfig.length !== null)
            Object.assign(config, localConfig)

        if (!('IntersectionObserver' in window)) {
            loadImagesImmediately(images)
        } else {
            observer = new IntersectionObserver(onIntersection, config)
            
            for (let i = 0; i < images.length; i++) {
                const image = images[i]
                if (image.classList.contains(config.imageLoadedClass))
                    continue

                observer.observe(image)
            }
        }
    },
    load: selector => {
        const imagesToLoad = document.querySelectorAll(selector)

        loadImagesImmediately(imagesToLoad);       
    },
    polyfill: () => {
        arrayFrom()
        objectAssign()
    }
}